import gulp from 'gulp'
import autoprefixer from 'gulp-autoprefixer'
import fileinclude from 'gulp-file-include'
import browserSync from 'browser-sync'

browserSync.create()

/* PATHS */

const path = {
	src: {
		html: './src/pages/*.html',
		css: './src/styles/*.css',
		js: './src/js/*.js',
	},
	prod: {
		self: './prod/',
		html: './prod/pages/',
		css: './prod/styles/',
		js: './prod/js/',
	},
}

/* PATHS */

const updatedHtml = () =>
	gulp
		.src(path.src.html)
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file',
			}),
		)
		.pipe(gulp.dest(path.prod.html))
        .pipe(browserSync.stream());

const updatedCss = () =>
	gulp
		.src(path.src.css)
		.pipe(autoprefixer({ cascade: false }))
		.pipe(gulp.dest(path.prod.css))
        .pipe(browserSync.stream());


const watcher = () => {
	browserSync.init({
		server: path.prod.html,
	})	
	gulp.watch(path.src.html, updatedHtml).on('change', browserSync.reload)
	gulp.watch(path.src.css, updatedCss).on('change', browserSync.reload)
}

gulp.task('default', gulp.series(updatedHtml, updatedCss, watcher))
