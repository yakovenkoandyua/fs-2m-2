import gulp from 'gulp'
import autoprefixer from 'gulp-autoprefixer'

/* PATHS */

const path = {
	src: {
		html: './src/pages/*.html',
		css: './src/styles/*.css',
		js: './src/js/*.js',
	},
	prod: {
		self: './prod/',
		html: './prod/pages/',
		css: './prod/styles/',
		js: './prod/js/',
	}
}

/* PATHS */



const updatedHtml = () => gulp.src('./index.html').pipe(gulp.dest('./prod/'))

const updatedCss = () =>
	gulp
		.src('./style.css')
		.pipe(autoprefixer({cascade: false,}),)
		.pipe(gulp.dest('./prod/'))

gulp.task('default', gulp.series(updatedHtml, updatedCss))
